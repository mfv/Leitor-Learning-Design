import { HomePage } from './../home/home';
import { InitialPage } from './../initial/initial';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  pushMain: any;
  constructor(public navCtrl: NavController ){
    this.pushMain = HomePage;    
  }
}
