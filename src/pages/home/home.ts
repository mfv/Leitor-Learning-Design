import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  tabReports:any;  
  tabMorphological:any;
  tabQuotes:any;  
  tabOthers:any;
  constructor(public navCtrl: NavController) {
    
  }

}
